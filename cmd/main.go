package main

import (
	"log"
	"net/http"

	"gitlab.com/Elevarup/dispatch/internal/controllers/foo"
	"gitlab.com/Elevarup/dispatch/internal/dispatch"
)

const (
	port = ":8090"
)

func main() {

	dispatch := &dispatch.Dispatch{
		Listener: make(chan func(), 2),
	}

	server := http.Server{
		Addr:    port,
		Handler: http.DefaultServeMux,
	}

	http.HandleFunc("/bar", foo.Handler{}.Foo(dispatch))

	go dispatch.Start()

	log.Println("server run, port ", port)
	err := server.ListenAndServe()
	if err != http.ErrServerClosed {
		log.Println("server, error: ", err)
	}
}
