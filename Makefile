BIN=bin/exec
BIN_RACE=bin/exec_race

run: ${BIN}
	./${BIN}

race: ${BIN_RACE}
	./${BIN_RACE}

build:
	go build -o ${BIN} cmd/*

build_race:
	go build --race -o ${BIN_RACE} cmd/*

clean:
	rm -r bin/*
