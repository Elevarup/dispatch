package foo

import (
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/Elevarup/dispatch/internal/dispatch"
	"gitlab.com/Elevarup/dispatch/internal/timer"
)

type (
	Handler struct {
	}
)

func (h Handler) Foo(d *dispatch.Dispatch) func(res http.ResponseWriter, req *http.Request) {

	return func(res http.ResponseWriter, req *http.Request) {
		t := req.URL.Query().Get("timer")
		timerQuery, err := strconv.Atoi(t)
		if err != nil {
			log.Println("error, parse timer: ", err)
			res.WriteHeader(http.StatusBadRequest)
			return
		}

		d.Add(func() {
			start := time.Now()
			timer.Timer{}.Sleep(timerQuery)
			log.Println("end: ", time.Since(start))
		})

	}
}
