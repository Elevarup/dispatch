package dispatch

type (
	Dispatch struct {
		Listener chan func()
	}
)

// Add - agregar una función al canal
func (d *Dispatch) Add(fun func()) {
	d.Listener <- fun
}

func (d *Dispatch) Start() {
	for {
		select {
		case fun := <-d.Listener:
			d.run(fun)
		}
	}
}

func (d *Dispatch) run(fun func()) {
	go fun()
}
