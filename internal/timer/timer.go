package timer

import "time"

type (
	Timer struct{}
)

func (t Timer) Sleep(timeSleep int) {
	time.Sleep(time.Second * time.Duration(timeSleep))
}
